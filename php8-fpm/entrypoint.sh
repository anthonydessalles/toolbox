#!/bin/sh

# Managing external hosts
HOST_IP=$(/sbin/ip route|awk '/default/ { print $3 }')

hostess add externalhost "$HOST_IP"

exec $@
