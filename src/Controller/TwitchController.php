<?php

namespace App\Controller;

use App\Client\TwitchClient;
use App\Form\TwitchType;
use App\Manager\DatacenterManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TwitchController extends Controller
{
    /**
     * @Route("/twitch/oauth", name="twitch_oauth")
     */
    public function twitchOauthAction(TwitchClient $twitchClient, Request $request, DatacenterManager $datacenterManager)
    {
        $form = $this
            ->createForm(TwitchType::class)
            ->add('search', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ])
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $search = $data['igdb'];

            return $this->redirectToRoute('twitch_search', [
                'igdb' => $search,
            ]);
        }

        return $this->render('twitch/twitch_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/twitch/search/{igdb}", name="twitch_search")
     */
    public function twitchSearchAction($igdb, TwitchClient $twitchClient, DatacenterManager $datacenterManager)
    {
        $searchTitle = $igdb;

        return $this->render('twitch/twitch.html.twig', [
            'search' => $searchTitle,
            'games' => $twitchClient->getGames($searchTitle),
            'datacenterIGDBNames' => $datacenterManager->getDatacenterIGDBGames(),
        ]);
    }
}
