<?php

namespace App\Controller;

use App\Manager\AWSManager;
use App\Manager\DatacenterManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AWSController extends Controller
{
    /**
     * @Route("/aws/index", name="aws_index")
     * @Template("aws/aws_index.html.twig")
     */
    public function awsIndexAction(AWSManager $awsManager)
    {
        return [
            'buckets' => $awsManager->getBuckets(),
        ];
    }

    /**
     * @Route("/aws/{bucket}", name="aws_bucket")
     * @Template("aws/aws_bucket.html.twig")
     */
    public function awsBucketAction($bucket, AWSManager $awsManager)
    {
        return [
            'bucket' => $bucket,
            'files' => $awsManager->getBucketFiles($bucket),
        ];
    }

    /**
     * @Route("/aws/sync/{bucket}/{prefix}", name="aws_sync_bucket_prefix")
     */
    public function awsSyncBucketPrefixAction($bucket, $prefix, DatacenterManager $datacenterManager)
    {
        $datacenterManager->syncAWSFiles($bucket, $prefix);

        return $this->redirectToRoute('aws_bucket', [
            'bucket' => $bucket,
        ]);
    }
}
