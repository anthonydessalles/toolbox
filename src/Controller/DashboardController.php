<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="dashboard")
     * @Template("dashboard.html.twig")
     */
    public function dashboardAction()
    {
        $folder = __DIR__ . '/../../assets/files';
        $files = [];

        if (is_dir($folder)) {
          foreach (scandir($folder) as $key => $file) {
            if ($key > 1) {
              $files[$file] = round(filesize($folder. "/" . $file) * 0.000001, 1);
            }
          }
        }

        return [
          'files' => $files,
        ];
    }
}
