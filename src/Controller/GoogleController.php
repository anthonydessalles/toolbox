<?php

namespace App\Controller;

use App\Form\GoogleType;
use App\Manager\DatacenterManager;
use App\Manager\GoogleManager;
use Google\Photos\Types\Album;
use Google_Service_YouTube_PlaylistListResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

class GoogleController extends Controller
{
    /**
     * @Route("/google/callback", name="google_callback")
     */
    public function googleCallbackAction(
        Request $request,
        DatacenterManager $datacenterManager,
        GoogleManager $googleManager,
        TranslatorInterface $translator
    ) {
        $state = $request->get('state');
        $form = $this
            ->createForm(GoogleType::class, [
                'state' => $state,
            ])
            ->add('synchronize', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ])
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $type = $data['type'];
            $from = $data['from'];
            $to = $data['to'];
            $code = $request->get('code');
            $title = $data['title'];
            $source = $data['source'];

            switch ($type) {
                case GoogleManager::GOOGLE_CALENDAR:
                    return $this->render('google/google_calendar.html.twig', [
                        'results' => $datacenterManager->syncCalendarEvents($code, $from, $to, $source),
                        'datacenterActivityEventIds' => $datacenterManager->getDatacenterActivityEventIds($from, $to),
                    ]);
                    break;
                case GoogleManager::GOOGLE_PHOTOS:
                    return $this->render('google/google_photos.html.twig', [
                        'results' => $datacenterManager->syncPhotos($code, $from, $to, $source),
                        'datacenterFilenames' => $datacenterManager->getDatacenterFilenames('/api/sources/3'),
                    ]);
                    break;
                case GoogleManager::GOOGLE_PHOTOS_ALBUMS:
                    return $this->render('google/google_photos_albums.html.twig', [
                        'results' => $datacenterManager->syncPhotosAlbums($code, $source),
                        'datacenterAlbumTitles' => $datacenterManager->getDatacenterAlbumTitles(),
                    ]);
                    break;
                case GoogleManager::GOOGLE_PHOTOS_COLLECTION_ALBUMS:
                    return $this->render('google/google_photos_collection_albums.html.twig', [
                        'results' => $googleManager->getPhotosCollectionAlbumsResults($code),
                    ]);
                    break;
                case GoogleManager::YOUTUBE_PLAYLISTS:
                    $results = $datacenterManager->syncYouTubePlaylists($code, $state, $source);
                    return $this->render('google/youtube_playlists.html.twig', [
                        'youTubePlaylists' => $results['youTubePlaylists'],
                        'nextPageToken' => $results['nextPageToken'],
                        'datacenterPlaylistsTitles' => $datacenterManager->getDatacenterPlaylistsTitles(),
                    ]);
                    break;
            }
        }

        return $this->render('google/google_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/google/oauth", name="google_oauth")
     */
    public function googleOauthAction(Request $request, GoogleManager $googleManager)
    {
        $state = $request->get('state') ?: null;

        return $this->redirect(filter_var($googleManager->getGoogleClient($state)->createAuthUrl(), FILTER_SANITIZE_URL));
    }
}
