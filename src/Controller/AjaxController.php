<?php

namespace App\Controller;

use App\Manager\DatacenterManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AjaxController extends Controller
{
    /**
     * @Route("/ajax/synchronize-activity", name="ajax_synchronize_activity")
     */
    public function ajaxSynchronizeActivity(Request $request, DatacenterManager $datacenterManager)
    {
        return new JsonResponse($datacenterManager->syncActivity(
            $request->get('eventId'),
            $request->get('name'),
            $request->get('start'),
            $request->get('end')
        ));
    }

    /**
     * @Route("/ajax/synchronize-file", name="ajax_synchronize_file")
     */
    public function ajaxSynchronizeFile(Request $request, DatacenterManager $datacenterManager)
    {
        return new JsonResponse($datacenterManager->syncFile(
            $request->get('filename'),
            $request->get('path'),
            $request->get('source'),
            (null !== $request->get('size')) ? $request->get('size') : null
        ));
    }

    /**
     * @Route("/ajax/synchronize-game", name="ajax_synchronize_game")
     */
    public function ajaxSynchronizeGame(Request $request, DatacenterManager $datacenterManager)
    {
        return new JsonResponse($datacenterManager->syncGame(
            $request->get('name'),
            $request->get('first_release_date'),
            $request->get('id'),
            $request->get('url')
        ));
    }
}
