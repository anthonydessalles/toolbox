<?php

namespace App\Controller;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Manager\DatacenterManager;

class FileController extends Controller
{
    /**
     * @Route("/{file}/read_json", name="read_json_file")
     * @Template("file/read_json_file.html.twig")
     */
    public function readJsonFileAction($file)
    {
        $fileContent = file_get_contents(__DIR__ . '/../../assets/files/' . $file);
        $jsonArrayContent = json_decode($fileContent, true);
        $jsonArrayContent['fileName'] = $file;

        return $jsonArrayContent;
    }

    /**
     * @Route("/{file}/raw", name="read_raw_file")
     */
    public function readRawFile($file)
    {
      //Read the filename
      $filename = __DIR__ . '/../../assets/files/' . $file;

      //Check the file exists or not
      if(file_exists($filename)) {

        //Define header information
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: 0");
        header('Content-Disposition: attachment; filename="'.basename($filename).'"');
        header('Content-Length: ' . filesize($filename));
        header('Pragma: public');

        //Clear system output buffer
        flush();

        //Read the size of the file
        readfile($filename);

        //Terminate from the script
        die();
      }
    }

    /**
     * @Route("/{file}/read_txt", name="read_txt_file")
     * @Template("file/read_txt_file.html.twig")
     */
    public function readTxtFileAction($file, DatacenterManager $datacenterManager)
    {
        $fileContent = file(__DIR__ . '/../../assets/files/' . $file, FILE_IGNORE_NEW_LINES);

        $files = [];
        foreach ($fileContent as $content) {
            if (str_contains($content, '/')) {
                $currentKey = substr($content, 0, -1);
            }

            if (str_contains(substr($content, -4), '.')) {
                $contentToArray = explode(' ', trim($content));
                $size = array_shift($contentToArray);
                $filename = implode(' ', $contentToArray);
                $files[$filename . "|" . $size] = $currentKey;
            }
        }

        return [
            'fileName' => $file,
            'files' => $files,
            'datacenterFilenames' => $datacenterManager->getDatacenterFilenames(DatacenterManager::DATACENTER_AWS_SOURCE[$file]),
            'aws_source' => DatacenterManager::DATACENTER_AWS_SOURCE,
        ];
    }
}
