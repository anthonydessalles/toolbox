<?php

namespace App\Manager;

use App\Client\DatacenterClient;
use Google\Auth\Credentials\UserRefreshCredentials;
use Google\Auth\OAuth2;
use Google\Photos\Library\V1\FiltersBuilder;
use Google\Photos\Library\V1\PhotosLibraryClient;
use Google\Photos\Library\V1\PhotosLibraryResourceFactory;
use Google\Photos\Types\SharedAlbumOptions;
use Google\Type\Date;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_YouTube;

class GoogleManager
{
    const GOOGLE_CALENDAR = 'google_calendar';
    const GOOGLE_PHOTOS = 'google_photos';
    const GOOGLE_PHOTOS_ALBUMS = 'google_photos_albums';
    const GOOGLE_PHOTOS_COLLECTION_ALBUMS = 'google_photos_collection_albums';
    const YOUTUBE_PLAYLISTS = 'youtube_playlists';

    /** @var Google_Client */
    private $googleClient;

    /** @var DatacenterClient */
    private $datacenterClient;

    /**
     * GoogleManager constructor.
     * @param Google_Client $googleClient
     */
    public function __construct(Google_Client $googleClient, DatacenterClient $datacenterClient)
    {
        $this->googleClient = $googleClient;
        $this->datacenterClient = $datacenterClient;
    }

    /**
     * @param $code
     * @return PhotosLibraryClient
     * @throws \Google\ApiCore\ValidationException
     * @throws \Google_Exception
     */
    private function getPhotosLibraryClient($code)
    {
        $client = $this->getGoogleClient();
        $oauth2 = new OAuth2([
            'clientId' => $client->getClientId(),
            'clientSecret' => $client->getClientSecret(),
            'authorizationUri' => 'https://accounts.google.com/o/oauth2/v2/auth',
            'redirectUri' => $client->getRedirectUri(),
            'tokenCredentialUri' => 'https://www.googleapis.com/oauth2/v4/token',
            'scope' => $client->getScopes(),
        ]);

        $oauth2->setCode($code);
        $authToken = $oauth2->fetchAuthToken();
        $refreshToken = $authToken['access_token'];
        $authCredentials = new UserRefreshCredentials(
            $client->getScopes(),
            [
                'client_id' => $client->getClientId(),
                'client_secret' => $client->getClientSecret(),
                'refresh_token' => $refreshToken
            ]
        );

        return new PhotosLibraryClient(['credentials' => $authCredentials]);
    }

    /**
     * @param null $state
     * @return Google_Client
     * @throws \Google_Exception
     */
    public function getGoogleClient($state = null)
    {
        $client = $this->googleClient;
        $client->setAuthConfig(__DIR__.'/../../config/client_secrets.json');
        $client->addScope('https://www.googleapis.com/auth/calendar.readonly');
        $client->addScope('https://www.googleapis.com/auth/photoslibrary.readonly');
        $client->addScope('https://www.googleapis.com/auth/youtube.readonly');

        if ($state) {
            $client->setState($state);
        }

        return $client;
    }

    /**
     * @param $code
     * @param \DateTime $from
     * @param \DateTime $to
     * @return array
     * @throws \Google_Exception
     */
    public function getCalendarResults($code, \DateTime $from, \DateTime $to)
    {
        $client = $this->getGoogleClient();
        $client->authenticate($code);
        $service = new Google_Service_Calendar($client);

        $results = [];
        $events = $service->events->listEvents('primary', [
            'orderBy' => 'startTime',
            'singleEvents' => TRUE,
            'timeMin' => $from->format('c'),
            'timeMax' => $to->format('c'),
        ]);

        foreach ($events->getItems() as $event) {
            $results[] = [
                'name' => $event->summary,
                'start' => $event->start->dateTime,
                'end' => $event->end->dateTime,
                'eventId' => $event->id
            ];
        }

        return $results;
    }

    /**
     * @param $code
     * @return array
     * @throws \Google\ApiCore\ApiException
     * @throws \Google\ApiCore\ValidationException
     * @throws \Google_Exception
     */
    public function getPhotosResults($code, \DateTime $from, \DateTime $to)
    {
        $photosLibraryClient = $this->getPhotosLibraryClient($code);

        $filtersBuilder = new FiltersBuilder();
        $googleFrom = new Date();
        $googleFrom->setDay($from->format('d'));
        $googleFrom->setMonth($from->format('m'));
        $googleFrom->setYear($from->format('Y'));

        $googleTo = new Date();
        $googleTo->setDay($to->format('d'));
        $googleTo->setMonth($to->format('m'));
        $googleTo->setYear($to->format('Y'));

        $filtersBuilder->addDateRange($googleFrom, $googleTo);
        $response = $photosLibraryClient->searchMediaItems(
            ['filters' => $filtersBuilder->build()]
        );

        $results = [];
        foreach ($response->iterateAllElements() as $element) {
            $results[] = $element;
        }

        return $results;
    }

    /**
     * @param $code
     * @return array
     * @throws \Google\ApiCore\ApiException
     * @throws \Google\ApiCore\ValidationException
     * @throws \Google_Exception
     */
    public function getPhotosAlbumsResults($code)
    {
        $photosLibraryClient = $this->getPhotosLibraryClient($code);
        $response = $photosLibraryClient->listAlbums();

        $results = [];
        foreach ($response->iterateAllElements() as $element) {
            $results[] = $element;
        }

        return $results;
    }

    public function getPhotosCollectionAlbumsResults($code)
    {
        $photosLibraryClient = $this->getPhotosLibraryClient($code);
        $albumsId = [
            'AEuqPfSwP_IIBi7Djsa-zjs4rq8LXy0nXXVGWiZ7U2A-yDS5iSu6Y4m0l0xGSB0gbsojPnPM1ayP',
            'AEuqPfRK2HLhMksHoLBQmkNmEhO4YfGjF3Iii1ZXoQDMHBeyye6_yER5sc5hqac1WvBvtHSYZCAb',
            'AEuqPfR5YE-ENNieIlrv5MEDbxGBIuNEV8q8WJly4D7ysHicCMWd13emaWfnxOlsTuRGYvA2QXPP',
            'AEuqPfQ274FKKkiC4VtX0hXvDQDs1gUbnHPT6Mcio_s2ijUvk9sp54AXuuTBq9bCDCmJChP5bECP',
            'AEuqPfQsBnvyoRB-bJ3LGsAi_M25qw-sW3CvLvh_Jc39IlrFfe37ldWALvsdxPOAKI4k2MlZPcf_',
            'AEuqPfS1oAYhI8BnqwBdl6WdGXXy7MqJgEzN6zQSLAX4j6hvg-gM1ehifVAbME1WGOQkBxMUQHfW',
            'AEuqPfRP25ASJOdjt7IMJ4AWKg8QXDdk77g_bGLI3viJEAsohUXuSYYdjOHrkvlyquMekKrXtyeQ',
            'AEuqPfRlR7au3Iimz0CsEG9-3dm3zd-KCI5r60lGtQfJDnESylopH8w53yU8B7ZLziBM7RyE3FcH',
            'AEuqPfS_g6IL-o4cBdvd8BTby5xEo-nrcq5vBwpmnHc-kBw8mRnZa_gRP-k8fKPxahtBuXk5ewE1',
            'AEuqPfSkpiC5GTBf05vrAWa-KdKruZ3o8ZRuX-T6bwx9cQAMtxUQHnFwdjz5W7yJNowSbaTIOHlb',
            'AEuqPfTSFeqBP5G8X4ABSDOvphgHF3UonRchWgOS1MyUh1DcyMt3DMhEMjq5VCebbjVS-v0GDLEu',
            'AEuqPfQsgeDrRCCX6s2WW3X-13pFrDE1xgoYwduL2D2W6IcaUOhcG59Uw8EJNTZc9Vw0VBQibYS-',
            'AEuqPfTkHg8OE2hm-sxJGxTXRM8mgUaUk0gCNgJLUXcYYnSPvye2-fFpyNy53-62sntjC1RHRNQ6',
            'AEuqPfTbQNu_nxzi1P3guY-7fDuWj8xdpQN93vNDgreDAkWh-LPuykz8dpL7PPT_ewwohx0gfDls',
            'AEuqPfTZLYC8RmsUlDl9wGmOOBRGTtlqecUIzzUGBVr6n-I8p_-Fd5avGPmlxToRjhkmN-rzPr_P',
            'AEuqPfQJhRNB_JYmVsgPNIUQ17cOd9TjE-ZgG7afKJB5-VGfphaU_fPaRvYL7vMEkcZNzKfuUhJ_',
            'AEuqPfR1ocZ0OR16vEzTg6HkughIRf6NOThYYhUzlAhG5-6RZ58mBNnOTxD5pLcx_Aw3x-w8GVdp',
            'AEuqPfQuYrrfouPu5UoA7u_IOtZrQOP2tR2k3_unYqUwLKA4JNfA9slHnAys4on2Ni2kqlNGfJSw',
            'AEuqPfSCmLdZMH6qb3f9f0ckdUA-VQkYfAkPDkbZZPC3JqI2o0u87C33I5RLCNkxbBIV1H30c3BL',
            'AEuqPfSLXkdH6SpNX-jTlVSnbjCFLnlQz97QfkzWmygEeuSAWoIkPtA_RYzLXdvmA2P0b7r4kgZV',
            'AEuqPfTZKVtihL20ENGj1bMoDdmO17fBXgbEI_Mcju7xbL0Ko1f1s_oSxKnE29BEIo76c7f4nDQu',
            'AEuqPfRwxZ5381GgdeI7VXeCCg7W21NsPBPrUzJ2ea9sl3LdpHGyQJFXoZvEcn_QH5OWDtIKdTng',
            'AEuqPfQLV_c4y19TvYF0QP2bae_9IZ3YXxf6dgWz1GWJdYsJXYQMZLY2SP0HWtQRQqh3cHyYCIRN',
            'AEuqPfSZpHgK404qw8oCyICzcH4Rs5D6LDAo8nSq2YdzNu1eVgc646OzDefZM5BXLx-AjBhoYaIf',
            'AEuqPfRNlKSrPppPajlEoqOMw7MKkft2qw-a3hJ9M0f3cDNi9aMHshLCd0c24zzkT1HjE8b3ALeH',
            'AEuqPfQGIqS--XobjUJeglcEbPsTDwLhGmjNqMSj6cFQNN5l5AeMOUppWeaYeWyAecG6MgQspLxb',
            'AEuqPfSOu-ocE37JFNm7MeNFWzpKniDDlx9CJe3nP1YNRLGUzJr2kVUDYvm92Nwlule-SPKT_Bw-',
            'AEuqPfSYe4ts9En7EwtEZ9aRhb2dRyEPUaS8o7c6AY_N8DI6n3EDQtG_cn2J42E9Iw3YfU4aUnY0',
            'AEuqPfTZD39Q1qIjAActyKg1yZ2kP_tVLWz87CwnCojit6FfvNFUZu7O1jknfL-FDhPmAlO4jdjx',
            'AEuqPfSy4I-CuqBXt76WOLvw27DAx3uNTUoMQq7QTnaPfG4iAdp-khheq2ay_A_4WkXTDdleQuHk',
            'AEuqPfTrsLm0XW3hbPTg1bk4TM1I_ci9JfPX2kCm12NWMH2gWRzQLoMVTmcrUSYoSiibzStVDsqE',
            'AEuqPfSELY_ikompg_DMPe17Wvh9lgYwq_DTAK19m06MsLejF6AbPOP7heqp_qgjESr53brMEp7K',
        ];

        $results = [];
        foreach ($albumsId as $id) {
            $response = $photosLibraryClient->searchMediaItems(['albumId' => $id]);
            foreach ($response->iterateAllElements() as $item) {
                $results[$id][] = $item;
            }
        }

        return $results;
    }

    /**
     * @param $code
     * @param null $nextPageToken
     * @return \Google_Service_YouTube_PlaylistListResponse
     * @throws \Google_Exception
     */
    public function getYouTubePlaylists($code, $nextPageToken = null)
    {
        $client = $this->getGoogleClient();
        $accessToken = $client->fetchAccessTokenWithAuthCode($code);
        $client->setAccessToken($accessToken);

        $service = new Google_Service_YouTube($client);
        $queryParams = [
            'maxResults' => 15,
            'mine' => true
        ];

        if ($nextPageToken) {
            $queryParams['pageToken'] = $nextPageToken;
        }

        return $service->playlists->listPlaylists('snippet,contentDetails,status,player', $queryParams);
    }
}
