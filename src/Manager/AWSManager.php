<?php

namespace App\Manager;

use App\Client\DatacenterClient;
use Aws\S3\S3Client;

class AWSManager
{
    /** @var S3Client */
    private $s3Client;

    /** @var DatacenterClient */
    private $datacenterClient;

    /**
     * AWSManager constructor.
     * @param S3Client $s3Client
     * @param DatacenterClient $datacenterClient
     */
    public function __construct(S3Client $s3Client, DatacenterClient $datacenterClient)
    {
        $this->s3Client = $s3Client;
        $this->datacenterClient = $datacenterClient;
    }

    /**
     * @return array
     */
    public function getBuckets()
    {
        $myBuckets = [];
        $buckets = $this->s3Client->listBuckets();

        foreach ($buckets['Buckets'] as $bucket) {
            $myBuckets[] = $bucket['Name'];
        }

        return $myBuckets;
    }

    /**
     * @param $bucket
     * @param null $prefix
     * @return array
     */
    public function getBucketFiles($bucket, $prefix = null)
    {
        $options['Bucket'] = $bucket;

        if ($prefix) {
            $options['Prefix'] = $prefix;
        }

        $files = [];
        foreach ($this->s3Client->getPaginator('ListObjects', $options) as $result) {
            foreach ($result['Contents'] as $object) {
                $split = explode('/', $object['Key']);
                $filename = array_pop($split);
                $path = implode('/', $split);

                if ($prefix) {
                    $files[] = $filename;
                } else {
                    $files[$path][] = $filename;
                }
            }
        }

        return $files;
    }
}