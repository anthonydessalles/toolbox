<?php

namespace App\Manager;

use App\Client\DatacenterClient;
use Google\Photos\Types\Album;
use Google\Photos\Types\MediaItem;
use Google_Service_YouTube_PlaylistListResponse;

class DatacenterManager
{
    const DATACENTER_AWS_SOURCE = [
        "thdd_main.txt" => "/api/sources/1",
        "games.anthony-dessalles.com" => "/api/sources/4",
        "library.anthony-dessalles.com" => "/api/sources/7",
        "thdd_main_backup.txt" => "/api/sources/9",
        "thdd_photos_backup.txt" => "/api/sources/10",
    ];
    const DATACENTER_SOURCES_PRE = "/api/sources/";

    /** @var DatacenterClient */
    private $datacenterClient;

    /** @var AWSManager */
    private $awsManager;

    /** @var GoogleManager */
    private $googleManager;

    /**
     * DatacenterManager constructor.
     * @param DatacenterClient $datacenterClient
     * @param AWSManager $awsManager
     * @param GoogleManager $googleManager
     */
    public function __construct(DatacenterClient $datacenterClient, AWSManager $awsManager, GoogleManager $googleManager)
    {
        $this->datacenterClient = $datacenterClient;
        $this->awsManager = $awsManager;
        $this->googleManager = $googleManager;
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getDatacenterActivityEventIds(\DateTime $from, \DateTime $to)
    {
        $activityEventIds = [];

        foreach ($this->datacenterClient->getActivities($from, $to) as $activity) {
            $activityEventIds[] = $activity->eventId;
        }

        return $activityEventIds;
    }

    public function getDatacenterAlbumTitles()
    {
        $albumTitles = [];

        foreach ($this->datacenterClient->getAlbums() as $album) {
            $albumTitles[] = $album->title;
        }

        return $albumTitles;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getDatacenterFilenames(string $source)
    {
        $datacenterFilenames = [];

        foreach ($this->datacenterClient->getFiles($source) as $file) {
            $datacenterFilenames[] = $file->filename;
        }

        return $datacenterFilenames;
    }

    public function getDatacenterIGDBGames()
    {
        $igdbNames = [];

        foreach ($this->datacenterClient->getGames() as $game) {
            if (isset($game->igdbId)) {
                $igdbNames[] = $game->igdbId;
            }
        }

        return $igdbNames;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getDatacenterPlaylistsTitles()
    {
        $datacenterPlaylists = [];

        foreach ($this->datacenterClient->getPlaylists() as $playlist) {
            $datacenterPlaylists[] = $playlist->title;
        }

        return $datacenterPlaylists;
    }

    /**
     * @param $eventId
     * @param $name
     * @param $start
     * @param $end
     * @return bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncActivity($eventId, $name, $start, $end, $source)
    {
        try {
            $this->datacenterClient->addActivity($eventId, $name, $start, $end, $source);

            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $title
     * @param $productUrl
     * @param $source
     * @param $externalId
     * @param null $shareableUrl
     * @return bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncAlbum($title, $productUrl, $source, $externalId, $shareableUrl = null)
    {
        try {
            $this->datacenterClient->addAlbum($title, $productUrl, $source, $externalId, $shareableUrl);

            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $bucket
     * @param $prefix
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncAWSFiles($bucket, $prefix)
    {
        $awsFiles = $this->awsManager->getBucketFiles($bucket, $prefix);

        foreach ($awsFiles as $awsFile) {
            $this->syncFile($awsFile, $prefix, DatacenterManager::DATACENTER_AWS_SOURCE[$bucket]);
        }

        return $awsFiles;
    }

    /**
     * @param $code
     * @param \DateTime $from
     * @param \DateTime $to
     * @return array
     * @throws \Google_Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncCalendarEvents($code, \DateTime $from, \DateTime $to, $source)
    {
        $results = $this->googleManager->getCalendarResults($code, $from, $to);

        foreach ($results as $result) {
            $this->syncActivity($result['eventId'], $result['name'], $result['start'], $result['end'], $source);
        }

        return $results;
    }

    /**
     * @param $filename
     * @param $path
     * @param $source
     * @param null $date
     * @param null $sourceUrl
     * @param null $mimeType
     * @param null $height
     * @param null $width
     * @param null $cameraMake
     * @param null $cameraModel
     * @param null $externalId
     * @return bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncFile(
        $filename,
        $path,
        $source,
        $size = null,
        $date = null,
        $sourceUrl = null,
        $mimeType = null,
        $height = null,
        $width = null,
        $cameraMake = null,
        $cameraModel = null,
        $externalId = null
    ) {
        try {
            $this->datacenterClient->addFile(
                $filename,
                $path,
                $source,
                $size,
                $date,
                $sourceUrl,
                $mimeType,
                $height,
                $width,
                $cameraMake,
                $cameraModel,
                $externalId
            );

            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function syncGame(
        $name,
        $date,
        $id,
        $url
    ) {
        try {
            $this->datacenterClient->addGame(
                $name,
                $date,
                $id,
                $url
            );

            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $code
     * @param \DateTime $from
     * @param \DateTime $to
     * @param $source
     * @return array
     * @throws \Google\ApiCore\ApiException
     * @throws \Google\ApiCore\ValidationException
     * @throws \Google_Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncPhotos($code, \DateTime $from, \DateTime $to, $source)
    {
        $results = $this->googleManager->getPhotosResults($code, $from, $to);

        /** @var MediaItem $result */
        foreach ($results as $result) {
            $filename = $result->getFilename();
            $path = '';
            $date = date('Y-m-d H:i:s', $result->getMediaMetadata()->getCreationTime()->getSeconds());
            $sourceUrl = $result->getProductUrl();
            $mimeType = $result->getMimeType();
            $height = $result->getMediaMetadata()->getHeight();
            $width = $result->getMediaMetadata()->getWidth();
            $cameraMake = $result->getMediaMetadata()->getPhoto() ? $result->getMediaMetadata()->getPhoto()->getCameraMake() : null;
            $cameraModel = $result->getMediaMetadata()->getPhoto() ? $result->getMediaMetadata()->getPhoto()->getCameraModel() : null;
            $externalId = $result->getId();

            $this->syncFile(
                $filename,
                $path,
                $source,
                null,
                $date,
                $sourceUrl,
                $mimeType,
                $height,
                $width,
                $cameraMake,
                $cameraModel,
                $externalId
            );
        }

        return $results;
    }

    /**
     * @param $code
     * @param $source
     * @return array
     * @throws \Google\ApiCore\ApiException
     * @throws \Google\ApiCore\ValidationException
     * @throws \Google_Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncPhotosAlbums($code, $source)
    {
        $results = $this->googleManager->getPhotosAlbumsResults($code);

        /** @var Album $result */
        foreach ($results as $result) {
            $this->syncAlbum(
                $result->getTitle(),
                $result->getProductUrl(),
                $source,
                $result->getId()
            );
        }

        return $results;
    }

    /**
     * @param $externalId
     * @param $title
     * @param $publishedAt
     * @param $status
     * @param $embedHtml
     * @param $source
     * @return bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncPlaylist($externalId, $title, $publishedAt, $status, $embedHtml, $source)
    {
        try {
            $this->datacenterClient->addPlaylist(
                $externalId,
                $title,
                $publishedAt,
                $status,
                $embedHtml,
                $source
            );

            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $code
     * @param null $nextPageToken
     * @param $source
     * @return array
     * @throws \Google_Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncYouTubePlaylists($code, $nextPageToken = null, $source)
    {
        /** @var Google_Service_YouTube_PlaylistListResponse $youTubePlaylists */
        $youTubePlaylists = $this->googleManager->getYouTubePlaylists($code, $nextPageToken);

        /** @var \Google_Service_YouTube_Playlist $result */
        foreach ($youTubePlaylists->getItems() as $result) {
            $this->syncPlaylist(
                $result->getId(),
                $result->getSnippet()->getTitle(),
                $result->getSnippet()->getPublishedAt(),
                $result->getStatus()->getPrivacyStatus(),
                $result->getPlayer()->getEmbedHtml(),
                $source
            );
        }

        return [
            'youTubePlaylists' => $youTubePlaylists->getItems(),
            'nextPageToken' => $youTubePlaylists->getNextPageToken(),
        ];
    }
}
