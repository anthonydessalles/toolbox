<?php

namespace App\Form;

use App\Manager\GoogleManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class GoogleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    GoogleManager::GOOGLE_CALENDAR => GoogleManager::GOOGLE_CALENDAR,
                    GoogleManager::GOOGLE_PHOTOS => GoogleManager::GOOGLE_PHOTOS,
                    GoogleManager::GOOGLE_PHOTOS_ALBUMS => GoogleManager::GOOGLE_PHOTOS_ALBUMS,
                    GoogleManager::GOOGLE_PHOTOS_COLLECTION_ALBUMS => GoogleManager::GOOGLE_PHOTOS_COLLECTION_ALBUMS,
                    GoogleManager::YOUTUBE_PLAYLISTS => GoogleManager::YOUTUBE_PLAYLISTS,
                ],
                'attr' => [
                    'class' => 'custom-select',
                ]
            ])
            ->add('source', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'data' => '/api/sources/2',
                ]
            ])
            ->add('title', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('state', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ],
                'empty_data' => $options['data']['state'],
            ])
            ->add('from', DateTimeType::class, [
                'date_widget' => 'single_text',
                'time_widget' => 'text',
                'data' => new \DateTime('last week'),
            ])
            ->add('to', DateTimeType::class, [
                'date_widget' => 'single_text',
                'time_widget' => 'text',
                'data' => new \DateTime(),
            ])
        ;
    }
}