<?php

namespace App\Client;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class DatacenterClient
{
    /** @var Client */
    private $client;

    /**
     * DatacenterClient constructor.
     * @param string $endpoint
     * @param string $username
     * @param string $password
     */
    public function __construct(string $endpoint, string $username, string $password)
    {
        $client = new Client();
        $response = $client->post($endpoint . 'login_check', [
            RequestOptions::JSON => [
                "username" => $username,
                "password" => $password
            ]
        ]);
        $token = json_decode($response->getBody(), true)['token'];

        $this->client = new Client([
            'base_uri' => $endpoint,
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ]
        ]);
    }

    /**
     * @param $eventId
     * @param $name
     * @param $start
     * @param $end
     * @param $source
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addActivity($eventId, $name, $start, $end, $source)
    {
        return $this->client->request('POST', 'activities', [
            RequestOptions::JSON => [
                "name" => $name,
                "start" => $start,
                "end" => $end,
                "eventId" => $eventId,
                "source" => $source,
            ]
        ]);
    }

    /**
     * @param $title
     * @param $productUrl
     * @param $source
     * @param $externalId
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addAlbum($title, $productUrl, $source, $externalId, $shareableUrl)
    {
        return $this->client->request('POST', 'albums', [
            RequestOptions::JSON => [
                "title" => $title,
                "productUrl" => $productUrl,
                "source" => $source,
                "externalId" => $externalId,
                "shareableUrl" => $shareableUrl,
            ]
        ]);
    }

    /**
     * @param $filename
     * @param $path
     * @param $source
     * @param null $size
     * @param null $date
     * @param null $sourceUrl
     * @param null $mimeType
     * @param null $height
     * @param null $width
     * @param null $cameraMake
     * @param null $cameraModel
     * @param null $externalId
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addFile(
        $filename,
        $path,
        $source,
        $size = null,
        $date = null,
        $sourceUrl = null,
        $mimeType = null,
        $height = null,
        $width = null,
        $cameraMake = null,
        $cameraModel = null,
        $externalId = null
    ) {
        $now = new \DateTime();

        return $this->client->request('POST', 'files', [
            RequestOptions::JSON => [
                "filename" => $filename,
                "path" => $path,
                "createdAt" => $now->format('Y-m-d H:i:s'),
                "source" => $source,
                "size" => $size,
                "date" => $date ?: $now->format('Y-m-d H:i:s'),
                "sourceUrl" => $sourceUrl,
                "mimeType" => $mimeType,
                "height" => $height,
                "width" => $width,
                "cameraMake" => $cameraMake,
                "cameraModel" => $cameraModel,
                "externalId" => $externalId
            ]
        ]);
    }

    public function addGame(
        $name,
        $date,
        $id,
        $url
    ) {
        $date = new \DateTime($date);

        return $this->client->request('POST', 'games', [
            RequestOptions::JSON => [
                "name" => $name,
                "slug" => self::slugify($name),
                "date" => $date->format('Y-m-d H:i:s'),
                "igdbId" => (int)$id,
                "igdbUrl" => $url,
                "isEnabled" => false
            ]
        ]);
    }

    /**
     * @param $externalId
     * @param $title
     * @param $publishedAt
     * @param $status
     * @param $embedHtml
     * @param $source
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addPlaylist(
        $externalId,
        $title,
        $publishedAt,
        $status,
        $embedHtml,
        $source
    ) {
        return $this->client->request('POST', 'playlists', [
            RequestOptions::JSON => [
                "externalId" => $externalId,
                "title" => $title,
                "publishedAt" => $publishedAt,
                "status" => $status,
                "embedHtml" => $embedHtml,
                "source" => $source
            ]
        ]);
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getActivities(\DateTime $from, \DateTime $to)
    {
        $url = 'activities?start[after]=' . $from->format('Y-m-dTH:i') . '&end[before]=' . $to->format('Y-m-dTH:i');

        return json_decode($this->client->request('GET', $url)->getBody());
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAlbums()
    {
        return json_decode($this->client->request('GET', 'albums')->getBody());
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getFiles(string $source)
    {
        $url = 'files?source=' . $source;

        return json_decode($this->client->request('GET', $url)->getBody());
    }

    public function getGames()
    {
        return json_decode($this->client->request('GET', 'games')->getBody());
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPlaylists()
    {
        return json_decode($this->client->request('GET', 'playlists')->getBody());
    }

    public static function slugify($text, string $divider = '-')
    {
        // replace non letter or digits by divider
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, $divider);

        // remove duplicate divider
        $text = preg_replace('~-+~', $divider, $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
