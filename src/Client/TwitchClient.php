<?php

namespace App\Client;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class TwitchClient
{
    private Client $client;
    private string $igbdEndpoint;

    public function __construct(string $oauthUrl, string $clientId, string $clientSecret, string $igbdEndpoint)
    {
        $this->igbdEndpoint = $igbdEndpoint;
        $client = new Client();
        $response = $client->post($oauthUrl, [
            RequestOptions::JSON => [
                "client_id" => $clientId,
                "client_secret" => $clientSecret,
                "grant_type" => "client_credentials",
            ]
        ]);
        $token = json_decode($response->getBody(), true)['access_token'];

        $this->client = new Client([
            'base_uri' => $igbdEndpoint,
            'headers' => [
                'Client-ID' => $clientId,
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ]
        ]);
    }

    public function getGames($search)
    {
        return json_decode($this->client->post($this->igbdEndpoint . '/games', [
            'body' => 'fields *; where id = ' . $search . ';'
        ])->getBody()->getContents());
    }
}
