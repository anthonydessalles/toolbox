Toolbox
=====================
Originally powered by Silex and now by [Symfony](https://symfony.com/) and [Docker](https://www.docker.com/). Based on this [repository](https://github.com/maxpou/docker-symfony). 

Build
-----

### Dev

    make start
    make stop

The website is accessible at [localhost](http://localhost/).

### Deploy

    git add -A
    git commit -m "v..."
    make deploy

The website is accessible at [toolbox.anthony-dessalles.com](http://toolbox.anthony-dessalles.com).